import re, os
from admin_settings import USER_CONFIG_STRINGS
def sip_reload():
    os.system("/bin/bash -c \"sudo asterisk -x \'sip reload\'\"")
def fetch_all_users(filename: str):
    users_db = []
    with open(filename) as f:
        textlist = f.readlines()
        for i in range(len(textlist) - USER_CONFIG_STRINGS):
            if textlist[i] == ";user\n":
                name = re.findall(r"(?<=\[)[^\]]+(?=\])", textlist[i+1])[0]
                secret = textlist[i+3].split("=")[1].strip()
                users_db.append({"name": name, "secret": secret})
    return users_db
def add_user_to_conf(filename: str, username: str, secret: str):
    with open(filename, 'a') as f:
        f.write(f";user\n[{username}](default_user_template)\ncallerid=\"{username}\" <{username}>\nsecret={secret}\n;enduser\n")
    sip_reload()
    return {"name": username, "secret": secret}
def remove_user_from_conf(filename: str, username: str):
    textlist = []
    with open(filename) as f:
        textlist = f.readlines()
        for i in range(len(textlist) - USER_CONFIG_STRINGS - 1):
            if textlist[i] == ";user\n" and f"[{username}]" in textlist[i+1]:
                first_index_del = i
                last_index_del = i + USER_CONFIG_STRINGS + 2
                del(textlist[first_index_del:last_index_del])
                break
    with open(filename, "w") as f:
        f.writelines(textlist)
    sip_reload()
    return {"name": username}
def edit_user_in_conf(filename: str, username: str, new_password: str):
    textlist = []
    with open(filename) as f:
        textlist = f.readlines()
        for i in range(len(textlist) - USER_CONFIG_STRINGS - 1):
            if textlist[i] == ";user\n" and f"[{username}]" in textlist[i + 1]:
                secret_index_edit = i + 3
                textlist[secret_index_edit] = f"secret={new_password}\n"
                break
    with open(filename, "w") as f:
        f.writelines(textlist)
    sip_reload()
    return {"name": username, "secret": new_password}