from fastapi import APIRouter, Request, Depends, Form
from fastapi.templating import Jinja2Templates
from typing import Annotated
from fastapi.security import HTTPBasicCredentials
from auth.utils import get_current_username
from scripts.parsers import fetch_all_users, add_user_to_conf,edit_user_in_conf,remove_user_from_conf
from admin_settings import SIP_CONFIG_PATH
from starlette.responses import RedirectResponse

router = APIRouter(
    prefix="",
    tags=["Pages"]
)
templates = Jinja2Templates(directory="templates")

@router.get("/")
def get_dashboard(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], request: Request):
    users_list = fetch_all_users(SIP_CONFIG_PATH)
    return templates.TemplateResponse("index.html", {"request": request, "users_list": users_list})
@router.get("/add")
def add_user_page(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], request: Request):
    return templates.TemplateResponse("add_user.html", {"request": request})
@router.post("/add")
def add_new_user(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], username: str = Form(...),
                 input_password: str = Form(...)):
    url = router.url_path_for('get_dashboard')
    users_db = fetch_all_users(SIP_CONFIG_PATH)
    added_user = dict()
    if len([user for user in users_db if user.get("name") == username]) == 0:
        add_user_to_conf(SIP_CONFIG_PATH,username,input_password)
    return RedirectResponse(url=url, status_code=303)
@router.get("/edit/{username}")
def edit_user_page(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], username: str, request: Request):
    users_db = fetch_all_users(SIP_CONFIG_PATH)

    user_data = [user for user in users_db if user.get("name") == username][0]
    return templates.TemplateResponse("edit_user.html", {"request": request, "user_data": user_data})
@router.post("/edit")
def save_edited_user(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], username: str = Form(...),
                     input_password: str = Form(...)):
    url = router.url_path_for('get_dashboard')
    users_db = fetch_all_users(SIP_CONFIG_PATH)
    for user in users_db:
        if user.get("name") == username:
            edit_user_in_conf(SIP_CONFIG_PATH, username, input_password)
    return RedirectResponse(url=url, status_code=303)
@router.post("/remove")
def save_edited_user(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], username: str = Form(...)):
    url = router.url_path_for('get_dashboard')
    users_db = fetch_all_users(SIP_CONFIG_PATH)
    for user in users_db:
        if user.get("name") == username:
            remove_user_from_conf(SIP_CONFIG_PATH, username)
    return RedirectResponse(url=url, status_code=303)