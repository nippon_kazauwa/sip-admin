from fastapi import FastAPI, Depends
from scripts.parsers import fetch_all_users, add_user_to_conf,edit_user_in_conf,remove_user_from_conf
from admin_settings import SIP_CONFIG_PATH
from fastapi.security import HTTPBasicCredentials
from fastapi.staticfiles import StaticFiles
from typing import Annotated
from auth.utils import get_current_username
from routers.router import router as router_pages

app = FastAPI()
app.include_router(router_pages)
app.mount("/static", StaticFiles(directory="static"), name="static")
@app.get("/users/all")
async def get_all_users(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)]):
    return fetch_all_users(SIP_CONFIG_PATH)
@app.get("/users/get_by_name")
async def get_user(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], user_name: str):
    users_db = fetch_all_users(SIP_CONFIG_PATH)
    return [user for user in users_db if user.get("name") == user_name]
@app.post("/users/add")
async def add_user(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], user_name: str, secret: str):
    users_db = fetch_all_users(SIP_CONFIG_PATH)
    added_user = dict()
    if len([user for user in users_db if user.get("name") == user_name]) == 0:
        added_user = add_user_to_conf(SIP_CONFIG_PATH,user_name,secret)
    return {"status": 200, "data": added_user}
@app.delete("/users/del_by_name")
async def del_user(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], user_name: str):
    users_db = fetch_all_users(SIP_CONFIG_PATH)
    removed_user = dict()
    for user in users_db:
        if user.get("name") == user_name:
            removed_user = remove_user_from_conf(SIP_CONFIG_PATH, user_name)
    return {"status": 200, "data": removed_user}
@app.patch("/users/edit/{user_name}")
async def edit_user(credentials: Annotated[HTTPBasicCredentials, Depends(get_current_username)], user_name: str, new_secret: str):
    users_db = fetch_all_users(SIP_CONFIG_PATH)
    edited_user = dict()
    for user in users_db:
        if user.get("name") == user_name:
            edited_user = edit_user_in_conf(SIP_CONFIG_PATH, user_name, new_secret)
    return {"status": 200, "data": edited_user}